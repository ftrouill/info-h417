#ifndef PROJECT_OUTPUTSTREAM_H
#define PROJECT_OUTPUTSTREAM_H


#include <iostream>
#include <fstream>
#include <string>
using namespace std;

void create();
void writeln(string line);
void close();

#endif //PROJECT_OUTPUTSTREAM_H
